﻿using System;
using Xamarin.Forms;

namespace Triggers.Utilities.Behaviors
{
    public class PasswordEntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Unfocused += OnEntryUnfocused;
            entry.Focused += OnEntryFocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Unfocused -= OnEntryUnfocused;
            entry.Focused -= OnEntryFocused;
            base.OnDetachingFrom(entry);
        }

        void OnEntryFocused(object sender, EventArgs e)
        {
            ((Entry)sender).Placeholder = "";
        }

        void OnEntryUnfocused(object sender, EventArgs e)
        {
            var entry = ((Entry)sender);
            entry.Placeholder = "Password";
            entry.PlaceholderColor = Color.Default;
        }
    }
}
