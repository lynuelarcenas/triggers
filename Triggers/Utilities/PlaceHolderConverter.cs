﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Triggers.Utilities
{
    public class PlaceHolderConverter : IValueConverter
    {
        public PlaceHolderConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if((string)value == "Required Field")
            {
                return true;
            }
            else if((string)value == "Password did not match")
            {
                return true;
            }
            else if ((string)value == "")
            {
                return true;
            }
            else if ((string)value == "Email" || (string)value == "Password" || (string)value == "Confirm Password")
            {
                return false;
            }
            else
            {
                return false;    
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
